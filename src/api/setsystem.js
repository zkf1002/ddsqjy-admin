import request from '@/utils/request'

const api = {
    // 系统设置
    Setting: '?s=App.Config.Setting',
    // 获取系统设置参数
    GetConfig: '?s=App.Config.Item',
    // 获取日志信息
    GetLog: '?s=App.Log.Page'
}

export default api
// 获取日志信息
export function GetLog (parameter) {
  return request({
    url: api.GetLog,
    method: 'post',
    data: parameter
  })
}
// 获取系统配置
export function GetConfig (parameter) {
  return request({
    url: api.GetConfig,
    method: 'post',
    data: parameter
  })
}
// 系统配置
export function Setting (parameter) {
  return request({
    url: api.Setting,
    method: 'post',
    data: parameter
  })
}