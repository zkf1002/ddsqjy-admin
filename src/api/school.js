import request from '@/utils/request'

const api = {
    // 获取学校列表
    getSchoolList: '?s=App.School.Page',
    // 添加学校
    AddSchool: '?s=App.School.InsertSchool',
    // 修改学校
    UpdataSchool: '?s=App.School.UpdateSchool',
    // 删除学校
    DelSchool: '?s=App.School.Del',
    // 获取学校标签列表
    GetLabelList: '?s=App.Category.Recursion',
    // 修改标签 (子标签)
    UpdataLabel: '?s=App.Category.Updatelable',
    // 添加学校标签
    AddLabel: '?s=App.Category.InsertCategory',
    // 删除子标签
    DelLabel: '?s=App.Category.Del',
    // 添加学校标签状态
    AddSchoolCate: '?s=App.Schoolcate.InsertCate',
    // 获取任务分类表
    GetTaskList: '?s=App.Taskcate.Page',
    // 添加任务分类
    AddTaskCate: '?s=App.Taskcate.InsertCate',
    // 修改任务分类
    UptadaTaskCate: '?s=App.Taskcate.Updatecate',
    // 删除任务分类
    DelTaskCate: '?s=App.Taskcate.Del',
    // 获取任务数据列表
    GetTaskData: '?s=App.Recode.Page',
    // 添加任务数据
    AddTaskData: '?s=App.Recode.InsertRecode',
    // 修改任务数据
    UpdataTaskData: '?s=App.Recode.UpdateRecode',
    // 删除任务数据
    DelTaskData: '?s=App.Recode.Del',
    // 获取任务列表
    GetTaskListOne: '?s=App.Task.Page',
    // 修改任务列表 
    UpdataTaskListOne: '?s=App.Task.UpdateTask',
    // 删除任务列表
    DelTaskListOne: '?s=App.Task.Del',
    // 修改动态表单列表
    UpdataTaskTwo: '?s=App.Task.UpdateForms',
    // 添加任务
    AddTask: '?s=App.Task.Add',
    // 学校用户列表
    GetSchoolUser: '?s=App.Schooltask.Task',
    // 获取列表
    GetStatus: '?s=App.Task.Getstatus',
    // 获取任务列表
    GetItem: '?s=App.Task.Item',
    // 新增任务数据
    AddRecode: '?s=App.Recode.InsertRecode',
    // 删除任务数据接口
    DelRecode: '?s=App.Recode.Del',
    // 获取全部任务接口
    GetAllSchoolTask: '?s=App.Schooltask.Page',
    // 驳回
    DelRole: '?s=App.Schooltask.Updatestate'
}

export default api
 // 驳回
 export function DelRole (parameter) {
  return request({
    url: api.DelRole,
    method: 'post',
    data: parameter
  })
}
 // 获取全部任务接口
 export function GetAllSchoolTask (parameter) {
  return request({
    url: api.GetAllSchoolTask,
    method: 'post',
    data: parameter
  })
}
 // 删除任务数据接口
 export function DelRecode (parameter) {
  return request({
    url: api.DelRecode,
    method: 'post',
    data: parameter
  })
}
 // 新增任务数据
 export function AddRecode (parameter) {
  return request({
    url: api.AddRecode,
    method: 'post',
    data: parameter
  })
}
 // 获取列表
 export function GetItem (parameter) {
  return request({
    url: api.GetItem,
    method: 'post',
    data: parameter
  })
}
 // 获取列表
 export function GetStatus (parameter) {
  return request({
    url: api.GetStatus,
    method: 'post',
    data: parameter
  })
}
 // 学校用户列表
 export function GetSchoolUser (parameter) {
  return request({
    url: api.GetSchoolUser,
    method: 'post',
    data: parameter
  })
}
// 添加任务
export function AddTask (parameter) {
  return request({
    url: api.AddTask,
    method: 'post',
    data: parameter
  })
}
// 修改动态表单列表
export function UpdataTaskTwo (parameter) {
  return request({
    url: api.UpdataTaskTwo,
    method: 'post',
    data: parameter
  })
}
// 删除任务列表
export function DelTaskListOne (parameter) {
  return request({
    url: api.DelTaskListOne,
    method: 'post',
    data: parameter
  })
}
// 修改任务列表
export function UpdataTaskListOne (parameter) {
  return request({
    url: api.UpdataTaskListOne,
    method: 'post',
    data: parameter
  })
}
// 获取任务列表
export function GetTaskListOne (parameter) {
  return request({
    url: api.GetTaskListOne,
    method: 'post',
    data: parameter
  })
}
// 删除任务数据列表
export function DelTaskData (parameter) {
  return request({
    url: api.DelTaskData,
    method: 'post',
    data: parameter
  })
}
// 修改任务数据列表
export function UpdataTaskData (parameter) {
  return request({
    url: api.UpdataTaskData,
    method: 'post',
    data: parameter
  })
}
// 添加任务数据列表
export function AddTaskData (parameter) {
  return request({
    url: api.AddTaskData,
    method: 'post',
    data: parameter
  })
}
// 获取任务数据列表
export function GetTaskData (parameter) {
  return request({
    url: api.GetTaskData,
    method: 'post',
    data: parameter
  })
}
// 删除任务分类
export function DelTaskCate (parameter) {
  return request({
    url: api.DelTaskCate,
    method: 'post',
    data: parameter
  })
}
// 修改任务分类
export function UptadaTaskCate (parameter) {
  return request({
    url: api.UptadaTaskCate,
    method: 'post',
    data: parameter
  })
}
// 添加任务分类
export function AddTaskCate (parameter) {
  return request({
    url: api.AddTaskCate,
    method: 'post',
    data: parameter
  })
}
// 获取任务分类
export function GetTaskList (parameter) {
  return request({
    url: api.GetTaskList,
    method: 'post',
    data: parameter
  })
}
// 添加学校标签状态
export function AddSchoolCate (parameter) {
  return request({
    url: api.AddSchoolCate,
    method: 'post',
    data: parameter
  })
}
// 删除学校标签
export function DelLabel (parameter) {
  return request({
    url: api.DelLabel,
    method: 'post',
    data: parameter
  })
}
// 修改学校标签
export function UpdataLabel (parameter) {
  return request({
    url: api.UpdataLabel,
    method: 'post',
    data: parameter
  })
}
// 获取学校标签
export function AddLabel (parameter) {
  return request({
    url: api.AddLabel,
    method: 'post',
    data: parameter
  })
}
// 获取学校标签列表
export function GetLabelList (parameter) {
  return request({
    url: api.GetLabelList,
    method: 'post',
    data: parameter
  })
}
// 删除学校
export function DelSchool (parameter) {
  return request({
    url: api.DelSchool,
    method: 'post',
    data: parameter
  })
}
// 修改学校
export function UpdataSchool (parameter) {
  return request({
    url: api.UpdataSchool,
    method: 'post',
    data: parameter
  })
}
// 添加学校
export function AddSchool (parameter) {
  return request({
    url: api.AddSchool,
    method: 'post',
    data: parameter
  })
}
// 获取学校列表
export function getSchoolList (parameter) {
  return request({
    url: api.getSchoolList,
    method: 'post',
    data: parameter
  })
}