import request from '@/utils/request'

const api = {
  // 资讯列表
  getArticle: '?s=App.Article.Page',
  // 新增资讯列表
  addArticle: '?s=App.Article.Save',
  // 删除咨询
  delArticle: '?s=App.Article.Del',
  // 审核资讯
  UpdataExamine: '?s=App.Article.UpdataExamine',

  // 资讯分类列表
  getArticletype: '?s=App.Articletype.Page',
  // 新增资讯列表
  addArticletype: '?s=App.Articletype.Save',
  // 删除资讯列表
  delArticletype: '?s=App.Articletype.Del',

  // 获取用户列表
  getUserList: '?s=App.User.Page',
  // 添加用户
  AddUser: '?s=App.User.Register',
  // 修改用户
  UpdataUser: '?s=App.User.Updateuser',
  // 删除用户
  DelUser: '?s=App.User.Del',

  // 获取部门列表
  getDepartmentList: '?s=App.Department.Page',
  // 添加部门
  AddDepartment: '?s=App.Department.Departinsert', 
  // 修改部门
  UpdataDepartment: '?s=App.Department.Departupdate',
  // 删除部门
  DelDepartment: '?s=App.Department.Del',
  // 修改部门状态
  OpenDepartment: '?s=App.Department.Updatestate',

  // 获取角色列表
  getRoLeList: '?s=App.Role.Page',
  // 添加角色
  AddRole: '?s=App.Role.InsertRole',
  // 修改角色
  UpdataRole: '?s=App.Role.InsertRole',
  // 删除角色
  DelRole: '?s=App.Role.Del',

  // 日志信息
  GetLog: '?s=App.Log.Page',

  // 获取校区列表
  getCampus: '?s=App.Campus.Page',
  
  // 上传图片
  getCredential:'?s=App.Examples_Upload.Go',
}

export default api
// 校区列表
export function getCampus (parameter) {
  return request({
    url: api.getCampus,
    method: 'post',
    data: parameter
  })
}

// 日志信息
export function GetLog (parameter) {
  return request({
    url: api.GetLog,
    method: 'post',
    data: parameter
  })
}

// 删除角色
export function DelRole (parameter) {
  return request({
    url: api.DelRole,
    method: 'post',
    data: parameter
  })
}
// 修改角色
export function UpdataRole (parameter) {
  return request({
    url: api.UpdataRole,
    method: 'post',
    data: parameter
  })
}
// 添加角色
export function AddRole (parameter) {
  return request({
    url: api.AddRole,
    method: 'post',
    data: parameter
  })
}
// 获取角色列表
export function getRoLeList (parameter) {
  return request({
    url: api.getRoLeList,
    method: 'post',
    data: parameter
  })
}

// 修改部门状态
export function OpenDepartment (parameter) {
  return request({
    url: api.OpenDepartment,
    method: 'post',
    data: parameter
  })
}
// 删除部门
export function DelDepartment (parameter) {
  return request({
    url: api.DelDepartment,
    method: 'post',
    data: parameter
  })
}
// 修改部门
export function UpdataDepartment (parameter) {
  return request({
    url: api.UpdataDepartment,
    method: 'post',
    data: parameter
  })
}
// 添加部门
export function AddDepartment (parameter) {
  return request({
    url: api.AddDepartment,
    method: 'post',
    data: parameter
  })
}
// 获取部门列表
export function getDepartmentList (parameter) {
  return request({
    url: api.getDepartmentList,
    method: 'post',
    data: parameter
  })
}

// 删除用户列表
export function DelUser (parameter) {
  return request({
    url: api.DelUser,
    method: 'post',
    data: parameter
  })
}
// 修改用户列表
export function UpdataUser (parameter) {
  return request({
    url: api.UpdataUser,
    method: 'post',
    data: parameter
  })
}
// 添加用户列表
export function AddUser (parameter) {
  return request({
    url: api.AddUser,
    method: 'post',
    data: parameter
  })
}
// 获取用户列表
export function getUserList (parameter) {
  return request({
    url: api.getUserList,
    method: 'post',
    data: parameter
  })
}

// 删除资讯列表
export function delArticletype (parameter) {
  return request({
    url: api.delArticletype,
    method: 'post',
    data: parameter
  })
}
// 新增咨询列表
export function addArticletype (parameter) {
  return request({
    url: api.addArticletype,
    method: 'post',
    data: parameter
  })
}
// 资讯分类列表
export function getArticletype (parameter) {
  return request({
    url: api.getArticletype,
    method: 'post',
    data: parameter
  })
}

// 审核资讯
export function UpdataExamine (parameter) {
  return request({
    url: api.UpdataExamine,
    method: 'post',
    data: parameter
  })
}
// 删除资讯列表
export function delArticle (parameter) {
  return request({
    url: api.delArticle,
    method: 'post',
    data: parameter
  })
}
// 新增资讯列表
export function addArticle (parameter) {
  return request({
    url: api.addArticle,
    method: 'post',
    data: parameter
  })
}
// 资讯列表
export function getArticle (parameter) {
  return request({
    url: api.getArticle,
    method: 'post',
    data: parameter
  })
}

// 上传图片
export function getCredential (parameter) {
  return request({
    url: api.getCredential,
    method: 'post',
    data: parameter
  })
}