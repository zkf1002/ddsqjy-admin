import request from '@/utils/request'

const api = {
    // 获取角色列表
    getRoLeList: '?s=App.Role.Page',
    // 添加角色
    AddRole: '?s=App.Role.InsertRole',
    // 修改角色权限
    UpdataRole: '?s=App.Role.UpdateRole',
    // 删除角色
    DelRole: '?s=App.Role.Del',
    // 获取用户列表
    getUserList: '?s=App.User.Page',
    // 添加用户
    AddUser: '?s=App.User.Register',
    // 修改用户
    UpdataUser: '?s=App.User.Updateuser',
    // 删除用户
    DelUser: '?s=App.User.Del',
    // 获取部门列表
    getDepartmentList: '?s=App.Department.Page',
    // 添加部门
    AddDepartment: '?s=App.Department.Departinsert',
    // 修改部门
    UpdataDepartment: '?s=App.Department.Departupdate',
    // 删除部门
    DelDepartment: '?s=App.Department.Del',
    // 学校列表添加学校标签
    SchoolRecursion: '?s=App.Category.Recursions',
    // 部门禁用 启用
    OpenDepartment: '?s=App.Department.Updatestate'
}

export default api
// 学校列表添加学校标签
export function SchoolRecursion (parameter) {
  return request({
    url: api.SchoolRecursion,
    method: 'post',
    data: parameter
  })
}
// 部门禁用
export function OpenDepartment (parameter) {
  return request({
    url: api.OpenDepartment,
    method: 'post',
    data: parameter
  })
}
// 删除部门
export function DelDepartment (parameter) {
  return request({
    url: api.DelDepartment,
    method: 'post',
    data: parameter
  })
}
// 修改部门
export function UpdataDepartment (parameter) {
  return request({
    url: api.UpdataDepartment,
    method: 'post',
    data: parameter
  })
}
// 添加部门
export function AddDepartment (parameter) {
  return request({
    url: api.AddDepartment,
    method: 'post',
    data: parameter
  })
}
// 获取部门列表
export function getDepartmentList (parameter) {
  return request({
    url: api.getDepartmentList,
    method: 'post',
    data: parameter
  })
}
// 删除用户信息
export function DelUser (parameter) {
  return request({
    url: api.DelUser,
    method: 'post',
    data: parameter
  })
}
// 修改用户信息
export function UpdataUser (parameter) {
  return request({
    url: api.UpdataUser,
    method: 'post',
    data: parameter
  })
}
// 添加用户
export function AddUser (parameter) {
  return request({
    url: api.AddUser,
    method: 'post',
    data: parameter
  })
}

// 获取用户列表
export function getUserList (parameter) {
  return request({
    url: api.getUserList,
    method: 'post',
    data: parameter
  })
}

// 删除一个角色权限
export function DelRole (parameter) {
  return request({
    url: api.DelRole,
    method: 'post',
    data: parameter
  })
}
// 修改一个角色权限
export function UpdataRole (parameter) {
  return request({
    url: api.UpdataRole,
    method: 'post',
    data: parameter
  })
}

// 添加一个角色
export function AddRole (parameter) {
  return request({
    url: api.AddRole,
    method: 'post',
    data: parameter
  })
}

// 获取角色列表
export function getRoLeList (parameter) {
  return request({
    url: api.getRoLeList,
    method: 'post',
    params: parameter
  })
}