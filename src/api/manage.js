import request from '@/utils/request'

const api = {
  user: '/user',
  role: '/role',
  service: '?s=App.User_User.GetList',
  permission: '/permission',
  permissionNoPager: '/permission/no-pager',
  orgTree: '/org/tree',
  UpdataFrom: '?s=App.User_User.Updatepwd',
  NewForm: '?s=App.User_User.Register',
  EditForm: '?s=App.User_User.Updateuser',
  DeleteForm: '?s=App.User_User.Deluser',
  FansList: '?s=App.Fans.FanList',
  UpdataFans: '?s=App.Fans.UpdateFan',
  DeleteFans: '?s=App.Fans.DeleteFan',
  AddFans: '?s=App.Fans.InsertFan',
  GetFansAddressList: '?s=App.Address.GetList',
  DeleteAddress: '?s=App.Address.DelAddress',
  AddAddress: '?s=App.Address.InsertAdd'
}

export default api

export function AddAddress (parameter) {
  return request({
    url: api.AddAddress,
    method: 'post',
    data: parameter
  })
}

export function DeleteAddress (parameter) {
  return request({
    url: api.DeleteAddress,
    method: 'post',
    data: parameter
  })
}

export function GetFansAddressList (parameter) {
  return request({
    url: api.GetFansAddressList,
    method: 'get',
    params: parameter
  })
}

export function AddFans (parameter) {
  return request({
    url: api.AddFans,
    method: 'post',
    data: parameter
  })
}

export function DeleteFans (parameter) {
  return request({
    url: api.DeleteFans,
    method: 'post',
    data: parameter
  })
}

export function UpdataFans (parameter) {
  return request({
    url: api.UpdataFans,
    method: 'post',
    data: parameter
  })
}

export function getFansList (parameter) {
  return request({
    url: api.FansList,
    method: 'get',
    params: parameter
  })
}

export function DeleteForm (parameter) {
  return request({
    url: api.DeleteForm,
    method: 'post',
    data: parameter
  })
}

export function EditForm (parameter) {
  return request({
    url: api.EditForm,
    method: 'post',
    data: parameter
  })
}

export function NewForm (parameter) {
  return request({
    url: api.NewForm,
    method: 'post',
    data: parameter
  })
}

export function getServiceList (parameter) {
  return request({
    url: api.service,
    method: 'get',
    params: parameter
  })
}

export function getUserList (parameter) {
  return request({
    url: api.user,
    method: 'get',
    params: parameter
  })
}

export function UpdataFrom (parameter) {
  return request({
    url: api.UpdataFrom,
    method: 'post',
    data: parameter
  })
}

export function getRoleList (parameter) {
  return request({
    url: api.role,
    method: 'get',
    params: parameter
  })
}

export function getPermissions (parameter) {
  return request({
    url: api.permissionNoPager,
    method: 'get',
    params: parameter
  })
}

export function getOrgTree (parameter) {
  return request({
    url: api.orgTree,
    method: 'get',
    params: parameter
  })
}

// id == 0 add     post
// id != 0 update  put
export function saveService (parameter) {
  return request({
    url: api.service,
    method: parameter.id === 0 ? 'post' : 'put',
    data: parameter
  })
}

export function saveSub (sub) {
  return request({
    url: '/sub',
    method: sub.id === 0 ? 'post' : 'put',
    data: sub
  })
}
