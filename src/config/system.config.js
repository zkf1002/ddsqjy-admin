/**
 * 系统配置文件
 */
/* eslint-disable */
// 系统配置 ，在模块中使用方法： this.$CONFIG.xxxx 
const config = {
    //系统名称
    systemTitle:'安全生产标准化管理体系网',
    //系统描述
    systemDescription:'业界领先的信息聚合平台',
    //系统底部 copyright@公司名称
    footerComName:'信息科技有限公司',
    //腾讯云存储
    Bucket: 'test-1256342487',
    Region: 'ap-chengdu',
    //后台接口地址
    serverUrl:'http://123.206.76.136/news/api'
    
  }
  export default config
