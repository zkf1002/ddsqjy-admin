// eslint-disable-next-line
import { UserLayout, BasicLayout, BlankLayout } from '@/layouts'

const RouteView = {
  name: 'RouteView',
  render: h => h('router-view')
}
export const asyncRouterMap = [
  {
    path: '/',
    name: 'dashboard',
    component: BasicLayout,
    meta: { title: '主页' },
    // redirect: '/ydiandames/release/index',
    children: [
      // {
      //   path: '*',
      //   component: () => import('@/views/404'),
      //   hidden: true
      // },
      //dashboard
      // {
      //   path: '/dashboard',
      //   name: 'dashboard',
      //   component: RouteView,
      //   redirect: '/dashboard/school',
      //   meta: { title: '欢迎页', icon: 'table' },
      //   children: [
      //     {
      //       path: '/dashboard/school',
      //       name: 'dashboardschool',
      //       component: () => import('@/views/dashboard/school/index'),
      //       meta: { title: '欢迎页', userTypeList:['dashboard1']}
      //     }
      //   ]
      // },
      // // system
      // {
      //   path: '/system',
      //   name: 'system',
      //   component: RouteView,
      //   redirect: '/system/roleList/index',
      //   meta: { title: '系统管理', icon: 'table', userTypeList:['system1']},
      //   children: [
      //     {
      //       path: '/system/roleList/index',
      //       name: 'SystemIndex',
      //       component: () => import('@/views/system/roleList/index'),
      //       meta: { title: '角色列表', userTypeList:['rolelist1']}
      //     },
      //     {
      //       path: '/system/users/index',
      //       name: 'SystemUsers',
      //       component: () => import('@/views/system/users/index'),
      //       meta: { title: '用户列表' , userTypeList:['userlist1']}
      //     },
      //     {
      //       path: '/system/department/index',
      //       name: 'SystemDepartment',
      //       component: () => import('@/views/system/department/index'),
      //       meta: { title: '部门列表' , userTypeList:['department1']}
      //     }
      //   ]
      // },
      // // school
      // {
      //   path: '/school',
      //   name: 'school',
      //   component: RouteView,
      //   redirect: '/school/information/index',
      //   meta: { title: '学校信息', icon: 'table', userTypeList:['school1']},
      //   children: [
      //     {
      //       path: '/school/information/index',
      //       name: 'Information',
      //       component: () => import('@/views/school/information/index'),
      //       meta: { title: '学校列表', userTypeList:['information1']}
      //     },
      //     {
      //       path: '/school/label/index',
      //       name: 'Label',
      //       component: () => import('@/views/school/label/index'),
      //       meta: { title: '学校标签', userTypeList:['label1']}
      //     }
      //   ]
      // },
      // // task
      // {
      //   path: '/task',
      //   name: 'task',
      //   component: RouteView,
      //   redirect: '/task/taskset/StepForm',
      //   meta: { title: '任务设置', icon: 'table', userTypeList:['task1'] },
      //   children: [
      //     {
      //       path: '/task/taskset/StepForm',
      //       name: 'taskSetting',
      //       component: () => import('@/views/task/taskset/StepForm'),
      //       meta: { title: '发布任务', userTypeList:['StepForm1']}
      //     },
      //     {
      //       path: '/task/tasklist/index',
      //       name: 'tasklist',
      //       component: () => import('@/views/task/tasklist/index'),
      //       meta: { title: '任务列表', userTypeList:['tasklist1']}
      //     },
      //     {
      //       path: '/task/classification/index',
      //       name: 'classification',
      //       component: () => import('@/views/task/classification/index'),
      //       meta: { title: '任务分类', userTypeList:['classification1']}
      //     },
      //     {
      //       path: '/task/schoolList/index',
      //       name: 'c',
      //       component: () => import('@/views/task/schoolList/index'),
      //       meta: { title: '当前任务', userTypeList:['schoolTask1']}
      //     },
      //     {
      //       path: '/task/taskdata/index',
      //       name: 'taskdataIndex',
      //       hidden: true,
      //       component: () => import('@/views/task/taskdata/index'),
      //       meta: { title: '数据列表' }
      //     },
      //     {
      //       path: '/task/UpdataForm',
      //       name: 'UpdataForm',
      //       hidden: true,
      //       component: () => import('@/views/task/UpdataForm'),
      //       meta: { title: '修改' }
      //     }
      //   ]
      // },
      // // set system
      // {
      //   path: '/setsystem',
      //   name: 'setsystem',
      //   component: RouteView,
      //   redirect: '/setsystem/system/index',
      //   meta: { title: '系统配置', icon: 'table', userTypeList:['setsystem1']},
      //   children: [
      //     {
      //       path: '/setsystem/system/index',
      //       name: 'setsystemindex',
      //       component: () => import('@/views/setsystem/system/index'),
      //       meta: { title: '系统设置', userTypeList:['setsystemindex1']}
      //     },
      //     {
      //       path: '/setsystem/log/index',
      //       name: 'logIndex',
      //       component: () => import('@/views/setsystem/log/index'),
      //       meta: { title: '日志信息', userTypeList:['logIndex1']}
      //     }
      //   ]
      // },

      // ymi dianda
      // message
      {
        path: '/ydiandames',
        name: 'ydiandames',
        component: RouteView,
        redirect: '/ydiandames/release/index',
        meta: { title: '信息资讯', icon: 'message', userTypeList:['ydiandames1']},
        children: [
          {
            path: '/ydiandames/release/index',
            name: 'ydiandamesrelease',
            component: () => import('@/views/ydiandames/release/index'),
            meta: { title: '资讯发布', userTypeList:['ydiandamesrelease1']}
          },
          {
            path: '/ydiandames/classification/index',
            name: 'ydiandamesclassification',
            component: () => import('@/views/ydiandames/classification/index'),
            meta: { title: '资讯分类', userTypeList:['ydiandamesclassification1']}
          }
        ]
      },
      // system
      {
        path: '/ydiandasystem',
        name: 'ydiandasystem',
        component: RouteView,
        redirect: '/ydiandasystem/users/index',
        meta: { title: '系统设置', icon: 'setting', userTypeList:['ydiandasystem1']},
        children: [
          {
            path: '/ydiandasystem/users/index',
            name: 'ydiandasyusers',
            component: () => import('@/views/ydiandasystem/users/index'),
            meta: { title: '用户管理', userTypeList:['ydiandasyusers1']}
          },
          {
            path: '/ydiandasystem/roleList/index',
            name: 'ydiandasyrole',
            component: () => import('@/views/ydiandasystem/roleList/index'),
            meta: { title: '角色管理', userTypeList:['ydiandasyrole1']}
          },
          {
            path: '/ydiandasystem/department/index',
            name: 'ydiandasydepartment',
            component: () => import('@/views/ydiandasystem/department/index'),
            meta: { title: '部门管理', userTypeList:['ydiandasydepartment1']}
          },
          {
            path: '/ydiandasystem/log/index',
            name: 'ydiandasylog',
            component: () => import('@/views/ydiandasystem/log/index'),
            meta: { title: '操作日志', userTypeList:['ydiandasylog1']}
          },
          {
            path: '/ydiandasystem/dictionary/index',
            name: 'ydiandasydictionary',
            component: () => import('@/views/ydiandasystem/dictionary/index'),
            meta: { title: '数据字典', userTypeList:['ydiandasydictionary1']}
          },
        ]
      }
    ]
  },
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import('@/views/user/Login')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import('@/views/user/Register')
      },
      {
        path: 'register-result',
        name: 'registerResult',
        component: () => import('@/views/user/RegisterResult')
      }
    ]
  },
]
