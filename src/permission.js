// import router from './router'
// import store from './store'
// import storage from 'store'
// import NProgress from 'nprogress' // progress bar
// import '@/components/NProgress/nprogress.less' // progress bar custom style
// import notification from 'ant-design-vue/es/notification'
// import { setDocumentTitle, domTitle } from '@/utils/domUtil'
// import { ACCESS_TOKEN } from '@/store/mutation-types'


// NProgress.configure({ showSpinner: false }) // NProgress Configuration

// const allowList = ['login', 'register', 'registerResult'] // no redirect allowList
// const loginRoutePath = '/user/login'
// const defaultRoutePath = '/dashboard/workplace'

// router.beforeEach((to, from, next) => {
//   NProgress.start() // start progress bar
//   to.meta && (typeof to.meta.title !== 'undefined' && setDocumentTitle(`${i18nRender(to.meta.title)} - ${domTitle}`))
//   /* has token */
//   if (storage.get(ACCESS_TOKEN)) {
//     if (to.path === loginRoutePath) {
//       next({ path: defaultRoutePath })
//       NProgress.done()
//     } else {
//       // check login user.roles is null
//       if (store.getters.roles.length === 0) {
//         // request login userInfo
//             const roles = JSON.parse(localStorage.getItem('role'));
//             // generate dynamic router
//             const permissionList = JSON.parse(localStorage.getItem('role'));
//             console.log(permissionList);
//             store.dispatch('GenerateRoutes', { roles,userTypeList:['admin'] }).then(() => {
//               console.log(1);
//               // 根据roles权限生成可访问的路由表
//               // 动态添加可访问路由表
//               console.log(store.getters.addRouters);
//               router.addRoutes(store.getters.addRouters)
//               // 请求带有 redirect 重定向时，登录自动重定向到该地址
//               const redirect = decodeURIComponent(from.query.redirect || to.path)
//               if (to.path === redirect) {
//                 // set the replace: true so the navigation will not leave a history record
//                 next({ ...to, replace: true })
//               } else {
//                 // 跳转到目的路由
//                 next({ path: redirect })
//               }
//             })
       
//       } else {
//         next()
//       }
//     }
//   } else {
//     if (allowList.includes(to.name)) {
//       // 在免登录名单，直接进入
//       next()
//     } else {
//       next({ path: loginRoutePath, query: { redirect: to.fullPath } })
//       NProgress.done() // if current page is login will not trigger afterEach hook, so manually handle it
//     }
//   }
// })

// router.afterEach(() => {
//   NProgress.done() // finish progress bar
// })




import Vue from 'vue'
import router from './router'
import store from './store'
import storage from 'store'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import notification from 'ant-design-vue/es/notification'
import { i18nRender } from '@/locales'
import {
  setDocumentTitle,
  domTitle
} from '@/utils/domUtil'
import {
  ACCESS_TOKEN
} from '@/store/mutation-types'

NProgress.configure({
  showSpinner: false
}) // NProgress Configuration
const loginRoutePath = '/user/login'
const whiteList = ['login', 'register', 'registerResult'] // no redirect whitelist
const defaultRoutePath = '/dashboard/school'

router.beforeEach((to, from, next) => {
  NProgress.start() // start progress bar
  to.meta && (typeof to.meta.title !== 'undefined' && setDocumentTitle(`${i18nRender(to.meta.title)} - ${domTitle}`))
  /* has token */
  if (localStorage.getItem('token')) {
    if (to.path === loginRoutePath) {
      next({ path: defaultRoutePath })
      NProgress.done()
    } else {
      if( store.getters.is_login && store.getters.is_loading ){
        console.log(1,router);
        next()
        NProgress.done()
        return 
      }
      // console.log(store.getters.is_loading)
      if( store.getters.is_loading ){
        next()
        NProgress.done()
        return 
      }
        const permissionList = JSON.parse(localStorage.getItem('role'))|| []; // 按钮控制
        const userTypeList = JSON.parse(localStorage.getItem('page')) || [];   //页面控制
        store.dispatch('Changeloading', {}).then(() => {
          store.dispatch('GenerateRoutes', {
            permissionList,
            userTypeList
          }).then(() => {
            console.log(store.getters.addRouters);
            router.addRoutes(store.getters.addRouters)
            const redirect = decodeURIComponent(from.query.redirect || to.path)
            console.log(to.path,redirect);
            if (to.path === redirect) {
              next({ ...to,
                replace: true
              })
              NProgress.done()
            } else {
              next({
                path: redirect
              })
              NProgress.done()
            }
          })
          .catch(() => {
            notification.error({
              message: '错误',
              description: '请求用户信息失败，请重试'
            })
            // 失败时，获取用户信息失败时，调用登出，来清空历史保留信息
            store.dispatch('Logout').then(() => {
              next({ path: loginRoutePath, query: { redirect: to.fullPath } })
            })
          })
        })
       
    }
  } else {
    if (whiteList.includes(to.name)) {
      // 在免登录白名单，直接进入
      next()
    } else {
      next({ path: loginRoutePath, query: { redirect: to.fullPath } })
      NProgress.done() // if current page is login will not trigger afterEach hook, so manually handle it
    }
  }
})




router.afterEach(() => {
  NProgress.done() // finish progress bar
})

/**
 * Action 权限指令
 * 指令用法：
 *  - 在需要控制 action 级别权限的组件上使用 v-action:[method] , 如下：
 *    <i-button v-action:add >添加用户</a-button>
 *    <a-button v-action:delete>删除用户</a-button>
 *    <a v-action:edit @click="edit(record)">修改</a>
 *
 *  - 当前用户没有权限时，组件上使用了该指令则会被隐藏
 *  - 当后台权限跟 pro 提供的模式不同时，只需要针对这里的权限过滤进行修改即可
 *
 *  @see https://github.com/sendya/ant-design-pro-vue/pull/53
 */
const action = Vue.directive('action', {
  bind: function(el, binding, vnode) {
    var actionName = binding.arg
    const permissions = localStorage.getItem('role')
    if (!permissions.includes(actionName)) {
      el.parentNode && el.parentNode.removeChild(el) || (el.style.display = 'none')
    }
  }
})

export {
  action
}
